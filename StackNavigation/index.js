import React, {Component} from 'react';

import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import MovieList from '../MovieList';
import Login from '../LoginScreen';
import Logout from '../LogoutBtn';
import Load from '../Loader';

import AsyncStorage from '@react-native-async-storage/async-storage';

class StackNavigation extends Component {
    // <Stack.Screen name="Loading" component={Load} options={{headerShown:false}} />
    
    render(){
        const Stack = createStackNavigator();
        return(
            <NavigationContainer>
                <Stack.Navigator initialRouteName="Login">
               
               
                
                <Stack.Screen name="Login" component={Login} options={{headerShown:false}} />
                
                <Stack.Screen name="Movie" component={MovieList} 
                options={({ navigation, route }) =>({
                    headerShown:true,
                    headerLeft: null,
                    headerRight: () => (
                        <Logout navigation={navigation}/>
                    )
                })} />

            
                </Stack.Navigator>
            </NavigationContainer>
        )
    }


}

export default StackNavigation