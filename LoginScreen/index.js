import React, {Component} from 'react';
import {View, Text, StyleSheet, Dimensions, Button} from 'react-native';
import { TextInput, TouchableOpacity } from 'react-native-gesture-handler';
import LinearGradient from 'react-native-linear-gradient';
import AsyncStorage from '@react-native-async-storage/async-storage';

class Login extends Component {

    async componentDidMount() {
        let userData = await AsyncStorage.getItem('user');
        console.log(userData)
        if(userData !==null){
            this.props.navigation.replace('Movie')
        }
    }

    state = {
        email: {
            value: '',
            isValid: true,
            errorMsg: '',
            regexErrorMsg: "please enter valid email",
            pattern: /^[a-zA-z\d\.-]*@[\w-]*\.[a-zA-Z]{2,3}(\.[a-zA-Z])?$/
        },
        password: {
            value: '',
            isValid: true,
            errorMsg: '',
            regexErrorMsg: "must contains 5 - 20 chars",
            pattern: /^[\w@-]{5,20}$/
        }
    }

    handleInput = async (key, text) => {
        let isValid = await this.validate(this.state[key].pattern, text);
        let errorMsg = await new Promise(resolve=> {
            if(text !== '' && isValid === false){
                resolve(this.state[key].regexErrorMsg)
            } else if (text === ''){
                resolve("required")
            } else {
                resolve("")
            }
        })
        await this.setState({
            ...this.state,
            [key] : {...this.state[key], value: text, isValid: isValid, errorMsg: errorMsg}
        })
    }

    validate = (patt, val) => {
        return patt.test(val)
    }

    submitLogin = async () => {
        if(this.state.email.value === 'email@gmail.com' && this.state.password.value === 'secret'){
            let userData = await this.getUserData(); 
            console.log(userData)
            await AsyncStorage.setItem('user', JSON.stringify(userData))
            let items = await AsyncStorage.getItem('user');
            console.log(items)
            await this.props.navigation.replace("Movie") 
        }
    }

    getUserData =() =>{
            return {
                name: 'Widya',
                email: this.state.email.value,
                password: this.state.password.value
            }
    }
    render(){
        
        return(
            <View style={styles.container}>
                
                <LinearGradient colors={['#2974FA', '#38ABFD', '#43D4FF']} style={styles.gradient}>
                    <View style={styles.formContainer}>
                        <View style={styles.formInputContainer}>
                            <TextInput onChangeText={(text) => this.handleInput("email", text)}
                            placeholder="someone@mail.com" 
                            style={styles.formInput} textAlign={'center'}
                             placeholderTextColor='#bfbfbf'/>
                             {
                                 this.state.email.isValid === false && ( <Text style={styles.error}>{this.state.email.errorMsg}</Text>)
                             }
                            
                        </View>
                        
                        <View style={styles.formInputContainer}>
                            <TextInput onChangeText={(text) => this.handleInput("password", text)}
                            placeholder="enter your password" style={styles.formInput} 
                            textAlign={'center'} secureTextEntry={true} 
                            placeholderTextColor='#bfbfbf'/>
                            {
                                this.state.password.isValid === false && ( <Text style={styles.error}>{this.state.password.errorMsg}</Text>)
                            }
                        </View>
                        <TouchableOpacity style={styles.formButton} onPress={this.submitLogin}>
                            <Text style={{color: 'white'}}>LOGIN</Text>
                        </TouchableOpacity>
                    </View>
                </LinearGradient>
            </View>
        )
    }
}

export default Login

const {width, height} = Dimensions.get("screen")

const styles = StyleSheet.create({
    container:{
        flex: 1
    },
    gradient: {
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    },
    formContainer:{
        width: width*0.8,
        // backgroundColor: 'white'
    },
    formInputContainer: {
        marginBottom: 20  
    },
    formInput: {
        color: 'white', 
        fontSize: 15,
        backgroundColor: '#09307c',
        borderRadius: 10,
        borderColor: 'darkblue',
        borderWidth: 1
    },
    formButton: {
        borderRadius: 10,
        fontWeight: 'bold',
        height: 50,
        width: width * 0.8,
        backgroundColor: '#2974FA',
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20
    },
    error:{
        color: 'black',
        fontStyle: 'italic',
        width: width * 0.8,
        textAlign: 'center',
    }
})