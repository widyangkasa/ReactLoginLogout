import React, {Component} from 'react';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { TouchableOpacity } from 'react-native-gesture-handler';
import {Text} from 'react-native';
class Logout extends Component {
    render() {
        return(
            <TouchableOpacity
                                onPress={ async () => {await AsyncStorage.clear(); this.props.reRenderStack; this.props.navigation.replace('Login');console.log(AsyncStorage.getItem('user'))}}
                                style={{marginRight: 20, color: 'blue'}}
                            ><Text>Logout</Text></TouchableOpacity>
        )
    }
}

export default Logout