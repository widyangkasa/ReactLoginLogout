import React, {Component} from 'react';
import {StyleSheet,View, Text, Image} from 'react-native';

class MovieItem extends Component {
    render(){
        const item = this.props.itemData;
        return(
            
            <View style={styles.MovieItemContainer} key={item.imdbID}>
                <Image source = {{uri: item.Poster}} style={{width: 90, height: 120}}/>
                <View style={styles.itemDetails}>
                    <Text style={styles.itemTitle}>{item.Title}</Text>
                    <Text style={styles.itemYear}>{item.Year}</Text>
                    <View style={styles.itemType}><Text style={{fontSize: 12}}>{item.Type}</Text></View>
                    
                    
                </View>
            </View>
        )
    }
}

export default MovieItem

const styles = StyleSheet.create({
    MovieItemContainer : {
        flex: 1,
        // backgroundColor: 'blue',
        paddingTop: 20,
        paddingBottom: 20,
        paddingLeft: 10,
        paddingRight: 10,
        borderTopColor: 'grey',
        borderTopWidth: 0.2,
        flexDirection: 'row'
    },
    itemDetails:{
        marginLeft: 15,
        flexDirection: 'column',

    },
    itemTitle:{
        fontWeight: 'bold',
        fontSize:17
        
    },
    itemType:{
        borderRadius: 20,
        borderWidth: 1,
        fontWeight: '300',
        alignSelf: 'flex-start',
        paddingLeft: 5,
        paddingRight: 5,
        justifyContent: 'center',
        marginTop: 5,
        borderColor: 'grey'

    }
})