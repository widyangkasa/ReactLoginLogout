import React, {Component} from 'react';
import {StyleSheet, View, Text, Keyboard} from 'react-native';
import axios from 'axios';

import MovieItem from '../MovieItem'
import { ScrollView, TextInput } from 'react-native-gesture-handler';
import AsyncStorage from '@react-native-async-storage/async-storage';


class MovieList extends Component {
    state = {
        search : 'avenger',
        movies : []
    }
    componentDidMount(){
        this.getMovieData(this.state.search)
           
        
    }

    getMovieData = async (input) =>{
        await axios.get(`http://www.omdbapi.com/?s=${input}&apikey=997061b4&`).then(res =>{
            console.log(res)
            if(res.data.Response === 'True') {
                this.setState({
                    ...this.state,
                    movies: res.data.Search,
                    search: input
                })
            } else{
                this.setState({
                    ...this.state,
                    movies:[],
                    search: input
                })
            }
        });
    }

    
    render(){
        let timer;
        let refreshData =(input) => {
           if(input!==''){
            timer = setTimeout(()=>{Keyboard.dismiss();this.getMovieData(input)}, 200)
           }else{
            timer = setTimeout(()=>{Keyboard.dismiss();this.getMovieData('avenger')}, 1000)
           }
            
        }
        
        return(
           <View style={{flex:1}}>
           {console.log(AsyncStorage.getItem('user'))}
           <View>
            <TextInput placeholder="Search" onChangeText={(input)=> { clearTimeout(timer); refreshData(input)}}/>
           </View>
            <ScrollView >
                {this.state.movies.length >0? this.state.movies.map((item, index)=>{
                    return(
                        // <Text>Hello</Text>
                            <MovieItem itemData = {item}/>             
                        )
                }): 
            
                    <Text>No item found</Text>
                }
            </ScrollView>
            </View>
        )
    }
}

export default MovieList